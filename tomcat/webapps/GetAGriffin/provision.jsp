<%@ page language="java" %>

<html>
<body>

<%

try {
	String deptNoStr = request.getParameter("DeptNo");
	if (deptNoStr == null) throw new Exception("No DeptNo specified");
	int deptNo = Integer.parseInt(deptNoStr);

	order.stub.OrderFactory orderFactory = new order.stub.OrderFactoryLive();
	//order.stub.OrderFactory orderFactory = new order.stub.OrderFactoryMock();
	order.stub.OrderStub orderStub = orderFactory.createOrder();
	try {
		String orderNo = orderStub.provision(deptNo);

		/* Display a success message, and the griffin id. The returned JSON is*/
		out.println("Order placed. Your Order Id is " + orderId + "<br />");

	} catch (Exception ex2) {
		out.println("An error occurred: " + ex2.getMessage() + "<br />");
		err.println("<pre>")
		ex2.printStackTrace(err);
		err.println("</pre>")
		out.println("<br /><br />")
	}

} catch (Exception ex) {
	out.println("<br />" + ex.getMessage() + "<br />");
}

/* Display a link to take the user back to the main page. */
out.println("<a href=\"index.html\">Return to main page</a>")

%>
</body>
</html>
